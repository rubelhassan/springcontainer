package me.rubel.springContainer;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/")
public class HomeController {
    Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Value("${test-property}")
    private String testProperty;

    private LogRepository logRepository;

    @GetMapping()
    public String home() {
        return "Welcome to Spring App Container. Test Property: " + testProperty;
    }

    @GetMapping("/log/{msg}")
    public ResponseEntity<Log> log(@PathVariable("msg") String message) {
        logger.info("Log Request: {}", message);
        Log log = logRepository.save(Log.builder().message(message).build());
        return ResponseEntity.ok(log);
    }

    @GetMapping("/log/all")
    public ResponseEntity<List<Log>> allLog() {
        return ResponseEntity.ok(logRepository.findAll());
    }

    @Autowired
    public void setLogRepository(LogRepository logRepository) {
        this.logRepository = logRepository;
    }
}
